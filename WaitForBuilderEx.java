public class Wait {
	private final Condition condition; // can probably leverage Selenium constants for this instead of local enum?
	private final Duration duration;

	public static class Builder{
		private Condition condition;
		private Duration duration;

		public Builder withCondition(Conditions val) {
			this.condition = val;
			return this;
		}		

		public Builder withDuration(Duration val) {
			this.duration = val;
			return this;
		}

		public Wait build() {

		}

		public void execute(){
			setDefaultDuration();

			// do just the wait

		}

		public <T extends RemoteWebElement> get(){
			setDefaultDuration();

			// wait and then return the object
		}

		private void setDefaultDuration(){
			if(this.duration == null){
				this.duration = Duration.ofSeconds(5);
			}
		}
	}

	private enum Condition {
		VISIBILITY ("visibility"){ 
			// code to wait for vis
		 },
		CLICABILITY {},
		STALENESS{},

		private String name;

		public Condition(String name)){
			this.name = name;
		}
	}
}

public class Find {
	private final By by;
	private final Selector selector;

	public static class Builder{
		private By by;
		private Selector selector;

		public Builder withBy(By by) {
			this.by = by;
			return this;
		}

		public Builder withSelector(Selector selector) {
			this.selector = selector;
			return this;
		}


}