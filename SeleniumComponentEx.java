public class ContentRail {
	public final Selector identifier = new Selector("data-qa-content-rail");
	public final Selector state = new Selector("data-qa-content-rail-state")

	private final BaseComponent component;

	public ContentRail() {
		this.component = new BaseComponent(identifier.getBy());
	}

	public boolean isOpen() {
		return component.getAttribute(state.getAttribute());
	}


	public void ensureOpen() {
		if(!isOpen()) {
			// click the filter in the header
			// is this good design? It almost feels as though the Rail knows about the Header
		}
	}

	public DualDatePicker getDatePicker() {
		ensureOpen();	
		return component.findChild(new DualDatePicker());
	}

	public Accordion getAccordionByName(String name) {
		ensureOpen();
		return component.findChild(new Accordion(name));
	}
}